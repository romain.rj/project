<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
			$table->foreignId('project_id');
			$table->foreignId('item_type_id');
			$table->foreignId('status_id');
			$table->foreignId('priority_id');
			$table->foreignId('user_id')->nullable();
			$table->string('title');
			$table->text('description')->nullable();
			$table->date('start')->nullable();
			$table->date('end')->nullable();
			$table->integer('weight')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
};
