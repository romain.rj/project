<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects') }}
        </h2>
		<a href="{{route('project.create')}}" class="float-right">{{ __('+ New project') }}</a>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
			<div class="max-w-xl">
				@foreach($projects as $project)
					<a href="/project/{{ $project->id }}" class="flex flex-col items-center bg-white border rounded-lg shadow-md md:flex-row md:max-w-xl hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
						<img class="object-cover w-full rounded-t-lg h-96 md:h-auto md:w-48 md:rounded-none md:rounded-l-lg" src="{{ $project->image }}" alt="{{ $project->name }}">
						<div class="flex flex-col justify-between p-4 leading-normal">
							<h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{{ $project->name }}</h5>
							<p class="mb-3 font-normal text-gray-700 dark:text-gray-400">{{ $project->description }}</p>
						</div>
					</a>
				@endforeach
			</div>
        </div>
    </div>
</x-app-layout>
