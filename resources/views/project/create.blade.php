<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('New project') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
			<div class="max-w-xl bg-white border rounded-lg shadow-md md:flex-row md:max-w-xl">
				<form>
					<label class="block">
						<span class="text-gray-700">Name</span>
						<input type="text" name="name" class="
							mt-0
							block
							w-full
							px-0.5
							border-0 border-b-2 border-gray-200
							focus:ring-0 focus:border-black
						  " />
					</label>
					
					<label class="block">
						<span class="text-gray-700">Description</span>
						<textarea name="description" class="
							mt-0
							block
							w-full
							px-0.5
							border-0 border-b-2 border-gray-200
							focus:ring-0 focus:border-black
						  " ></textarea>
					</label>
				</form>
				
				<label class="block">
					<span class="text-gray-700">Image</span>
					<input type="file" name="image" class="
						mt-0
						block
						w-full
						px-0.5
						border-0 border-b-2 border-gray-200
						focus:ring-0 focus:border-black
					  " />
				</label>
		
				<button type="submit" class="bg-sky-500 hover:bg-sky-700 text-white font-bold py-2 px-4 rounded-full">Save</button>
			</div>
        </div>
    </div>
</x-app-layout>
