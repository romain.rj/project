<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects') }} {{ $project->name }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div class="max-w-xl ">
					<form class="grid grid-cols-1 gap-6">
						
						<label class="block">
							<span class="text-gray-700">Type</span>
							<select class="
								block
								w-full
								mt-0
								px-0.5
								border-0 border-b-2 border-gray-200
								focus:ring-0 focus:border-black
							  ">
								@foreach($itemtypes as $type)
									<option>{{ $type->name }}</option>
								@endforeach
							</select>
						</label>
						
						<label class="block">
							<span class="text-gray-700">Status</span>
							<select class="
									block
									w-full
									mt-0
									px-0.5
									border-0 border-b-2 border-gray-200
									focus:ring-0 focus:border-black
								  ">
								@foreach($status as $status)
									<option>{{ $status->name }}</option>
								@endforeach
							</select>
						</label>
						
						<label class="block">
							<span class="text-gray-700">Priority</span>
							<select class="
									block
									w-full
									mt-0
									px-0.5
									border-0 border-b-2 border-gray-200
									focus:ring-0 focus:border-black
								  ">
								@foreach($priorities as $priority)
									<option>{{ $priority->name }}</option>
								@endforeach
							</select>
						</label>
						
						<label class="block">
							<span class="text-gray-700">Title</span>
							<input type="text" name="title" class="
								mt-0
								block
								w-full
								px-0.5
								border-0 border-b-2 border-gray-200
								focus:ring-0 focus:border-black
							  " />
						</label>
							  
							  
							  
						<label class="block">
							<span class="text-gray-700">Assigned</span>
							<select class="
									block
									w-full
									mt-0
									px-0.5
									border-0 border-b-2 border-gray-200
									focus:ring-0 focus:border-black
								  ">>
								@foreach($users as $user)
									<option>{{ $user->name }}</option>
								@endforeach
							</select>
						</label>
						
						
						<label class="block">
							<span class="text-gray-700">Weight</span>
							<input type="number" name="weight" class="
								mt-0
								block
								w-full
								px-0.5
								border-0 border-b-2 border-gray-200
								focus:ring-0 focus:border-black
							  " />
						</label>
						
						<label class="block">
							<span class="text-gray-700">Start</span>
							<input type="date" name="start" class="
								mt-0
								block
								w-full
								px-0.5
								border-0 border-b-2 border-gray-200
								focus:ring-0 focus:border-black
							  " >
						</label>
						<label class="block">
							<span class="text-gray-700">End</label>
							<input type="date" name="end" class="
								mt-0
								block
								w-full
								px-0.5
								border-0 border-b-2 border-gray-200
								focus:ring-0 focus:border-black
							  " >
						</label>
						---
						<label class="block">
							<span class="text-gray-700">Description</span>
							<textarea name="description" class="
								mt-0
								block
								w-full
								px-0.5
								border-0 border-b-2 border-gray-200
								focus:ring-0 focus:border-black
							  " ></textarea>
						</label>
						---
						Relationships
						---
						Comments
						<button type="submit" class="bg-sky-500 hover:bg-sky-700 text-white font-bold py-2 px-4 rounded-full">Save</button>
					</form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
