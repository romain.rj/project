<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects') }} 
			
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="w-96 mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div class="w-96">
				
					<x-project-table>
						<x-slot name="head">
							<th>Hello</th>
						</x-slot>
						
						<x-slot name="body"></x-slot>
					</x-project-table>
					
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
