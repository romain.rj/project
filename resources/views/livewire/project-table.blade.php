<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects') }} {{ $project->name }}
        </h2>
    </x-slot>
	
	<x-project-table>
	
		<x-slot name="head">
			<x-table.heading sortable>id</x-table.heading>
			<x-table.heading sortable>Type</x-table.heading>
			<x-table.heading sortable>Title</x-table.heading>
			<x-table.heading sortable>Priority</x-table.heading>
			<x-table.heading sortable>Status</x-table.heading>
			<x-table.heading sortable>Assigned</x-table.heading>
			<x-table.heading sortable>Weight</x-table.heading>
			<x-table.heading sortable>Start</x-table.heading>
			<x-table.heading sortable>End</x-table.heading>
		</x-slot>
		
		<x-slot name="body">
			@foreach($items as $item)
				<x-table.row>
					<x-table.cell>
						{{ $item->id }}
					</x-table.cell>
					<x-table.cell>
						<select name="type" class="block w-full mt-0 px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black">
							@foreach($itemtypes as $type)
								<option value="{{ $type->id }}" @if($type->id == $item->type->id) selected @endif>{{ $type->name }}</option>
							@endforeach
						</select>
					</x-table.cell>
					<x-table.cell>
						<span wire:click="edit">{{ $item->title }}</span>
					</x-table.cell>
					<x-table.cell>
						<select name="priority" class="block w-full mt-0 px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black">
							@foreach($priorities as $priority)
								<option value="{{ $priority->id }}" @if($priority->id == $item->priority->id) selected @endif>{{ $priority->name }}</option>
							@endforeach
						</select>
					</x-table.cell>
					<x-table.cell>
						<select name="status" class="block w-full mt-0 px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black">
							@foreach($statuses as $status) 
								<option value="{{ $status->id }}" 
								@if($status->id == $item->status->id) selected @endif>{{ $status->name }}</option>
							@endforeach
						</select>
					</x-table.cell>
					<x-table.cell>
						<select name="user" class="block w-full mt-0 px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black">
							<option></option>
							@foreach($users as $user)
								<option value="{{ $user->id }}" @if($user->id == $item->user->id) selected @endif>{{ $user->name }}</option>
							@endforeach
						</select>
					</x-table.cell>
					<x-table.cell>
						<input type="number" 
							class="mt-0 block w-full px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black"
							value="{{ $item->weight }}"
						/>
					</x-table.cell>
					<x-table.cell>
						<input type="date" 
							class="mt-0 block w-full px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black"
							value="{{ $item->start }}"
						/>
					</x-table.cell>
					<x-table.cell>
						<input type="date" 
							class="mt-0 block w-full px-0.5 border-0 border-b-2 border-gray-200 focus:ring-0 focus:border-black"
							value="{{ $item->end }}"
						/>
					</x-table.cell>
				</x-table.row>
			@endforeach
		</x-slot>
		
	</x-project-table>

	<x-modal.dialog wire:model="showEditModal">
		<x-slot name="title">Edit</x-slot>
		<x-slot name="content">
		test
		</x-slot>
		<x-slot name="footer">
			<x-primary-button>Save</x-primary-button>
		</x-slot>
	</x-modal.dialog>
	
</x-app-layout>