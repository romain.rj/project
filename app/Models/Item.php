<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Project;

class Item extends Model
{
    use HasFactory;
	
	protected $fillable = [
		'project_id',
		'item_type_id',
		'status_id',
		'priority_id',
		'user_id',
		'title',
		'description',
		'start',
		'end',
		'weight',
	];
	
	public function project()
    {
        return $this->belongsTo(Project::class);
    }
	
	public function user()
    {
        return $this->belongsTo(User::class);
    }
	
	public function type()
    {
        return $this->belongsTo(ItemType::class, "item_type_id");
    }
	
	public function priority()
    {
        return $this->belongsTo(Priority::class);
    }
	
	public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
