<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Project;
use App\Models\Item;
use App\Models\ItemType;
use App\Models\Status;
use App\Models\Priority;
use App\Models\User;
use Auth;

class ProjectTable extends Component
{
	public $showEditModal = false;
	
	public function show(Project $project)
	{
		return view('livewire.project-table', [
			'items'=>Item::where('project_id',$project->id)->get(),
			'project'=>$project,
			'itemtypes'=>ItemType::all(),
			'statuses'=>Status::all(),
			'priorities'=>Priority::all(),
			'users'=>User::all()
		]);
	}
	
	public function edit()
	{
		$this->showEditModal = true;
	}
	
    public function render()
    {
		return view('livewire.project-table');
    }
}
