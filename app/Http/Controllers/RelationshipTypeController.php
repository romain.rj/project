<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRelationshipTypeRequest;
use App\Http\Requests\UpdateRelationshipTypeRequest;
use App\Models\RelationshipType;

class RelationshipTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRelationshipTypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRelationshipTypeRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RelationshipType  $relationshipType
     * @return \Illuminate\Http\Response
     */
    public function show(RelationshipType $relationshipType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RelationshipType  $relationshipType
     * @return \Illuminate\Http\Response
     */
    public function edit(RelationshipType $relationshipType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRelationshipTypeRequest  $request
     * @param  \App\Models\RelationshipType  $relationshipType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRelationshipTypeRequest $request, RelationshipType $relationshipType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RelationshipType  $relationshipType
     * @return \Illuminate\Http\Response
     */
    public function destroy(RelationshipType $relationshipType)
    {
        //
    }
}
